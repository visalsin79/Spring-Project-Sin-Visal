package com.visal.spring.min.project.demo.repositories.providers;


import com.visal.spring.min.project.demo.models.Book;
import com.visal.spring.min.project.demo.models.Category;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {


    public String create_category(Category category) {
        return new SQL() {{
            INSERT_INTO("tb_category");
            VALUES("name", "#{name}");

        }}.toString();
    }

    public String getAllProvider(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON b.cate_id = c.id");
            ORDER_BY("b.id desc");
        }}.toString();
    }


    public String create(Book book){
        return new SQL(){{
            INSERT_INTO("tb_book");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("publisher", "#{publisher}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("cate_id" ,"#{category.id}");
        }}.toString();
    }





}
